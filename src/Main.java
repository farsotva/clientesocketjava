import clases.Empresa;
import com.google.gson.Gson;
import io.socket.client.IO;
import io.socket.client.Socket;

import java.net.URI;
import java.util.Arrays;

import static enums.Variables.*;

public class Main {


    public static void main(String[] args) {
        SocketConnection();
    }

    private static void SocketConnection() {


        try {
            URI uri = URI.create(IP + PORT);
            IO.Options options = IO.Options.builder()
                    //.setAuth(Collections.singletonMap("token", "123456"))
                    .setReconnection(true)
                    .build();

            Socket socket = IO.socket(uri, options);

            socket.on(Socket.EVENT_CONNECT, objects -> System.out.println("Conectado con exito"))
                    .on(Socket.EVENT_CONNECT_ERROR, objects -> System.out.println("Error al tratar de conectarse" + Arrays.toString(objects)))
                    .on(Socket.EVENT_DISCONNECT, objects -> System.out.println("Se ha desconectado la conexion"))
                    .on(EVENTO, objects -> {
                        String json = Arrays.toString(objects);
                        Gson gson = new Gson();
                        Empresa[] empresa = gson.fromJson(json, Empresa[].class);
                        System.out.println("Empresa recibida: " + empresa[0].getNombreCliente() + " - " + empresa[0].getNombreInscrito());
                    });

            socket.connect();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}