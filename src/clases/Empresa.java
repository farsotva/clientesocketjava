package clases;

public class Empresa {
    private String nombreCliente, nombreInscrito;


    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public String getNombreInscrito() {
        return nombreInscrito;
    }

    public void setNombreInscrito(String nombreInscrito) {
        this.nombreInscrito = nombreInscrito;
    }
}
